\documentclass[10pt,a4paper,prl,amsmath,amsfonts,reprint,nofootinbib,superscriptaddress,notitlepage]{revtex4-1}

\usepackage[T1]{fontenc} \usepackage{pstricks} \usepackage{subfigure}

\usepackage{bm}
\usepackage{graphicx}
\usepackage{physics}
\usepackage{mathtools}
\usepackage{color}
\newcommand{\bl}[1]{\textcolor{blue}{\textbf{#1}}}
\DeclareMathOperator{\sinc}{sinc}
\usepackage{physics}
\usepackage[colorlinks]{hyperref}

\hypersetup{
	bookmarksnumbered,
	pdfstartview={FitH},
	citecolor={darkgreen},
	linkcolor={darkblue},
	urlcolor={darkblue},
	pdfpagemode={UseOutlines}}
\definecolor{darkgreen}{RGB}{20,100,20}
\definecolor{darkblue}{RGB}{0,0,130}
\definecolor{darkred}{rgb}{.8,0,0}

\begin{document}
	\title{Fermionic quantum carpets: From canals and ridges to solitonlike structures -- Supplemental Materials}
	
	\author{Piotr T. Grochowski}
	\email{piotr@cft.edu.pl}
	\affiliation{Center for Theoretical Physics, Polish Academy of Sciences, Aleja Lotnik\'ow 32/46, 02-668 Warsaw, Poland}
	\author{Tomasz Karpiuk}
	\email{t.karpiuk@uwb.edu.pl}
	\affiliation{Wydzia{\l} Fizyki, Uniwersytet w Bia{\l}ymstoku,  ul. K. Cio{\l}kowskiego 1L, 15-245 Bia{\l}ystok, Poland}
	\author{Miros{\l}aw Brewczyk}
	\email{m.brewczyk@uwb.edu.pl}
	\affiliation{Wydzia{\l} Fizyki, Uniwersytet w Bia{\l}ymstoku,  ul. K. Cio{\l}kowskiego 1L, 15-245 Bia{\l}ystok, Poland}
	\author{Kazimierz Rz\k{a}{\.z}ewski}
	\email{kazik@cft.edu.pl}
	\affiliation{Center for Theoretical Physics, Polish Academy of Sciences, Aleja Lotnik\'ow 32/46, 02-668 Warsaw, Poland}
	
	\date{\today}
	\maketitle
\onecolumngrid
\appendix
\section{Derivation of $p$-th contribution}
\setcounter{equation}{0}
Let's focus on the $p$-th contribution coming from each orbital:
\begin{align}
d_p^n =  \sigma(p) \sum_{k=1}^{\infty}  \lambda(n,k) \lambda(n,k+|p|).
\end{align}
Firstly, we recall the Fourier transform
\begin{align}
\hat{f}(k) = \frac{1}{\sqrt{2 \pi}} \int_{-\infty}^{\infty} f(x) e^{i k x} \dd{x},
\end{align}
and assuming that we will consider only real functions, we introduce sine and cosine transforms:
\begin{align}
\hat{f}^s(k) = \mathcal{I} \hat{f}^(k), \ \ \ \hat{f}^c(k) = \mathcal{R} \hat{f}^(k).
\end{align}
We can immediately see that $\hat{f}^s(k) = - \hat{f}^s(-k)$.
Let's evaluate overlaps between initial orbitals and box eigenmodes:
\begin{align}
\lambda(n,k) = \int_{-\infty}^{\infty} \dd{x} \ \varphi_k^\star (x) \phi_k (x) = \sqrt{\frac{2}{L}} \int_{-\infty}^{\infty} \dd{x} \phi_n(x) \sin{\left( \frac{k \pi}{L} x\right)}=\sqrt{\frac{4 \pi}{L}}\widehat{\phi_n}^s\left(\frac{k \pi}{L}\right),
\end{align}
where $\phi_n(x)=\phi_n(x) \theta{\left( x \right)} \theta{ \left( L- x \right) }$ is meant to be truncated into the box with the width of $L$.
Therefore, we can write
\begin{align}
d_p^n = & \sigma(p) \sum_{k=1}^{\infty}  \frac{4 \pi}{L} \widehat{\phi_n}^s\left(\frac{k \pi}{L}\right) \widehat{\phi_n}^s\left(\frac{(k+|p|) \pi}{L}\right) \longrightarrow \nonumber \\
& 2  \sigma(p) \int_{-\infty}^{\infty} \dd{\tilde{k}} \widehat{\phi_n}^s\left(\tilde{k}\right) \widehat{\phi_n}^s\left(\frac{|p| \pi}{L}-\tilde{k}\right) =  2  \sigma(p) \  \widehat{\phi_n}^s \star \widehat{\phi_n} ^s\left(\frac{|p| \pi}{L}\right),
\end{align}
where $\star$ denotes usual convolution that in our case takes form:
\begin{align}
\hat{f}^s \star \hat{g}^s(l)&=\frac{1}{2 \pi}\int_{-\infty}^{\infty} \dd{k} \int_{-\infty}^{\infty} \dd{x} \int_{-\infty}^{\infty} \dd{y} f(x)\sin(kx) g(y) \sin((l-k)y) \nonumber \\
&=-\frac{1}{2}  \int_{-\infty}^{\infty} \dd{x} f(x) g(x) \cos(lx) + \frac{1}{2}  \int_{-\infty}^{\infty} \dd{x} f(x) g(-x) \cos(lx)
\end{align}
However, we consider only functions that vanish outside $x \in [0,L]$, so the above expression can be simplified into
\begin{align}
\hat{f}^s \star \hat{g}^s(l) =-\frac{\pi}{2} \widehat{f g }^c (l).
\end{align}
We arrive at the compact form for the $p$-th relative depth for a given orbital:
\begin{align}\label{reldeporb}
d_p^n =   2  \sigma(p) \  \widehat{\phi_n}^s \star \widehat{\phi_n} ^s\left(\frac{|p| \pi}{L}\right) = -\sigma(p) \sqrt{\frac{2}{\pi}} \widehat{\phi_n^2}^c \left(\frac{|p| \pi}{L}\right).
\end{align}
We can therefore write the whole $p$-th relative depth as
\begin{align}
d_p = -\sigma(p) \frac{1}{N} \sum_{n=1}^{N} \sqrt{\frac{2}{\pi}} \widehat{\phi_n^2}^c \left(\frac{|p| \pi}{L}\right) = -\sigma(p) \sqrt{\frac{2}{\pi}} \frac{1}{N} \widehat{\sum_{n=1}^{N} \phi_n^2}^c \left(\frac{|p| \pi}{L}\right) = -\sigma(p) \sqrt{\frac{2}{\pi}} \frac{1}{N} \widehat{n(x,0)}^c \left(\frac{|p| \pi}{L}\right),
\end{align}
where $n(x,0)$ is the initial one-particle density of the fermionic gas.

\section{Derivation of widths of the structures}
\setcounter{equation}{0}
In three dimensions, atoms are trapped in a box in $x$-direction and in an arbitrary perpendicular confinement:
\begin{align}
V(x,y,z)=\text{Box}(x)+V_y(y)+V_z(z).
\end{align}
As such, orbitals are now characterized by three independent quantum numbers, $n=(n_x,n_y,n_z)$.
Analogously to 1D case, we consider single-particle density, but integrated over perpendicular degrees of freedom:
\begin{align}
\left| \phi_n(x,t)\right|^2 = \int \dd z \int \dd y \left| \phi_n(x,y,z,t)\right|^2 = \left| \sum_{k=1}^{\infty} \lambda(n_x,k) \varphi_k (x) e^{-i E_k t / \hbar} \right|^2.
\label{ert}
\end{align}
Expression \eqref{ert} differs from its one-dimensional counterpart by changing $n$ to $n_x$ inside a sum.
Again, we introduce $p$-th contribution:
\begin{align}
n_p(x,t)  \approx - \sum_{n=1}^{N} \sum_{k=1}^{\infty} \frac{1}{L} \lambda(n_x,k) \lambda(n_x,k+|p|)\cos{\left( (2k+|p|)\frac{\pi}{L} (x- p v_0 t)\right) },
\label{sua}
\end{align}
but this time we can use explicit expressions for overlaps, as we have been considering a box potential in $x$-axis:
\begin{align}
\lambda(n,k)=
\begin{cases} 
\frac{2}{\pi} \sqrt{\frac{D}{L}}  (-1)^{n+1}\sin\left(\frac{k \pi D}{L}\right)  \frac{n}{n^2-\left( \frac{kD}{L}\right)^2 }, \ \  & n L \neq k D, \\
\sqrt{\frac{D}{L}}  ,\ \  & n L = k D.
\end{cases}
\end{align}
One can explicitly check that multiplication of functions $\lambda$ in \eqref{sua} can be approximated by
\begin{align}
\lambda(n_x,k) \lambda(n_x,k+|p|) \approx \frac{D}{L} \operatorname{sinc}\left( \frac{k \pi D}{L} - n_x\pi \right) \operatorname{sinc}\left( \frac{(k+|p|) \pi D}{L} - n_x\pi \right),
\end{align}
and is centered around
\begin{align}
k_0 = \frac{n_x L}{D} - \frac{|p|}{2}.
\end{align}
As a next step, we stick to region close to the structure's peaks, working with variables describing distance from them, $x_p=(x- p v_0 t)-x_0^p$.
Then, we identify slowly varying parts of $p$-th contribution in this region:
\begin{align}
n_p(x,t)  \approx & - \sum_{n=1}^{N} \sum_{k=1}^{\infty} \frac{1}{L} \frac{D}{L} \operatorname{sinc}\left( \frac{k \pi D}{L} - n_x\pi \right) \operatorname{sinc}\left( \frac{(k+|p|) \pi D}{L} - n_x\pi \right) \cos{\left( (2k+|p|)\frac{\pi}{L} (x- p v_0 t)\right) } \nonumber \\
\approx &-\sum_{n=1}^{N} \frac{1}{L} \cos{\left( (2k_0+|p|)\frac{\pi}{L} x_p\right) } \sum_{k=1}^{\infty} \frac{D}{L} \operatorname{sinc}\left( \frac{k \pi D}{L} - n_x\pi \right) \operatorname{sinc}\left( \frac{(k+|p|) \pi D}{L} - n_x\pi \right).
\label{asd}
\end{align}
Sum over $k$ in \eqref{asd} can be turned into an integral, that can be readily estimated:
\begin{align}
\int \dd k \ \frac{D}{L} \operatorname{sinc}\left( \frac{k \pi D}{L} - n_x\pi \right) \operatorname{sinc}\left( \frac{(k+|p|) \pi D}{L} - n_x\pi \right) = \operatorname{sinc}\left( \frac{\pi D}{L} |p| \right).
\end{align}
Within these approximations, we are left with expression for $p$-th contribution to the single-particle density:
\begin{align}
n_p(x_p,t) \approx &-\sum_{n=1}^{N} \frac{1}{L} \cos{\left( (2k_0+|p|)\frac{\pi}{L} x_p\right) } \operatorname{sinc}\left( \frac{\pi D}{L} |p| \right) = -\frac{1}{L} \operatorname{sinc}\left( \frac{\pi D}{L} |p| \right) \sum_{n=1}^{N} \cos{\left(2 n_x \pi \frac{  x_p}{D}\right) }.
\end{align}
An expression
\begin{align}
\sum_{n=1}^{N} \cos{\left(2 n_x  \pi \frac{ x_p}{D}\right) }
\label{hard}
\end{align}
can be readily calculated in one dimension, where 
\begin{align}
n_x(n)=n, \ \ \ \ n_{\text{max}}=N.
\end{align}
In this case, it is called Langrange formula and yields
\begin{align}
\sum_{n=1}^{N} \cos{\left(2 n  \pi \frac{ x_p}{D}\right) } = -\frac{1}{2}+\frac{\sin{\left( \left(N+\frac{1}{2} \right) 2 \pi \frac{x_p}{D}  \right) }}{2 \sin{\left(\frac{\pi x_p}{D} \right) }} \approx - N \sigma(p) \operatorname{sinc}\left(2 k_F x_p \right),
\end{align}
where $k_F$ is the initial Fermi wavevector of the gas:
\begin{align}
k_F = \frac{\pi N}{D}
\end{align}
Therefore, $p$-th contribution can be invoked in form
\begin{align}
n_p(x_p) \approx \frac{N}{L} \sigma(p)  \operatorname{sinc}\left( \frac{\pi D}{L} |p| \right)  \operatorname{sinc}\left(2 k_F x_p \right) = \frac{N}{L} d_p  \operatorname{sinc}\left(2 k_F x_p \right).
\end{align}
However, a sum \eqref{hard} cannot be explicitly calculated in three-dimensional case, but we find out that $p$-th contribution can be numerically approximated by
\begin{align}
n_p(x_p)\approx \frac{N}{L} d_p  \operatorname{sinc}\left(\eta k_F x_p \right),
\end{align}
where $\eta$ is a constant that depends on the character of perpendicular trapping.
The approximation is reasonably accurate for $x_p$ up to
\begin{align}
|x_p| \sim \frac{\pi}{\eta k_F}.
\end{align}
With this approximation of the shape, we can calculate widths of the half maximum for each solitonlike structure:
\begin{align}
w = \frac{w_0}{\eta k_F}, \ \ \ w_0=3.79098...
\end{align}
For different types of trappings, we have differents values of $k_F$ and $\eta$.
For box trap with the length of $D_y$ in $y$-direction and box trap with the length of $D_z$ in $z$-direction:
\begin{align}
k_F=\left(\frac{3}{4} \pi^2 \frac{N}{D D_y D_z} \right)^{1/3}, \ \ \ \ \eta \sim 3.2.
\end{align}
For harmonic trap with the length of $a_y=\sqrt{\frac{\hbar}{m \omega_y}}$ in $y$-direction and box trap with the length of $D_z$ in $z$-direction:
\begin{align}
k_F=\left(16 \pi \frac{m \omega_y}{\hbar}\frac{N}{D D_z} \right)^{1/4}, \ \ \ \ \eta \sim 1.4.
\end{align}
For harmonic trap with the length of $a_y=\sqrt{\frac{\hbar}{m \omega_y}}$ in $y$-direction and harmonic trap with the length of $a_z=\sqrt{\frac{\hbar}{m \omega_z}}$ in $z$-direction:
\begin{align}
k_F=\left(15 \pi \frac{m \omega_y}{\hbar} \frac{m \omega_z}{\hbar} \frac{N}{D} \right)^{1/5}, \ \ \ \ \eta \sim 1.3
\end{align}
\end{document}